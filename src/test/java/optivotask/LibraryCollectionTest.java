package optivotask;

import optivotask.model.Author;
import optivotask.model.Book;
import optivotask.model.Magazine;
import optivotask.model.Publication;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static junit.framework.Assert.*;
import static org.junit.Assert.assertNotNull;

/**
 * Test for {@link LibraryCollection}
 *
 * @author Igor Polietaiev
 */
public class LibraryCollectionTest {

    private static final int TOTAL_NUMBER_OF_PUBLICATIONS = 14;

    @Test
    public void testAuthorsMatching() {

        LibraryCollection libraryCollection = new LibraryCollection();
        Set<Publication> publications = libraryCollection.getAllPublications();
        for (Publication publication : publications) {
            assertFalse("Authors were not matched for publication with ISBN: " + publication.getIsbn(), publication.getAuthorsList().isEmpty());
        }
    }

    @Test
    public void testGetAllPublications() {
        LibraryCollection libraryCollection = new LibraryCollection();
        Set<Publication> publications = libraryCollection.getAllPublications();
        assertEquals("Publications were not loaded properly.", TOTAL_NUMBER_OF_PUBLICATIONS, publications.size());
    }

    @Test
    public void testGetPublicationByIsbn() {
        LibraryCollection libraryCollection = new LibraryCollection();
        String isbn = "4545-8558-3232";
        String title = "Schlank im Schlaf ";
        Publication publication = libraryCollection.getPublicationByIsbn(isbn);
        assertNotNull("Publication with the following ISBN was not found: " + isbn, publication);
        assertEquals("Publications were not loaded properly.", title, publication.getTitle());
    }

    @Test
    public void testGetPublicationByAuthor() {
        LibraryCollection libraryCollection = new LibraryCollection();
        Author author = new Author();
        author.setEmail("pr-lieblich@optivo.de");
        Set<Publication> publications = libraryCollection.getPublicationByAuthor(author);

        Book book1 = new Book();
        book1.setIsbn("2145-8548-3325");
        assertTrue("The following book belongs to author " + author.getEmail() + ", but was not found: " + book1, publications.contains(book1));

        Book book2 = new Book();
        book2.setIsbn("1024-5245-8584");
        assertTrue("The following book belongs to author " + author.getEmail() + ", but was not found: " + book2, publications.contains(book2));

        Book book3 = new Book();
        book3.setIsbn("2221-5548-8585");
        assertTrue("The following book belongs to author " + author.getEmail() + ", but was not found: " + book3, publications.contains(book3));

        Magazine magazine = new Magazine();
        magazine.setIsbn("2365-5632-7854");
        assertTrue("The following magazine belongs to author " + author.getEmail() + ", but was not found: " + magazine, publications.contains(magazine));

    }

    @Test
    public void testGetAllPublicationsOrderedByTitle() {
        LibraryCollection libraryCollection = new LibraryCollection();
        Set<Publication> orderedPublications = libraryCollection.getAllPublicationsOrderedByTitle();

        Set<Publication> allPublications = libraryCollection.getAllPublications();
        Set<String> orderedTitles = new TreeSet<String>();
        for (Publication publication : allPublications) {
            orderedTitles.add(publication.getTitle());
        }

        // test ordering
        List<String> orderedTitlesList = new ArrayList<String>(orderedTitles);
        int index = 0;
        for (Publication publication : orderedPublications) {
            assertEquals("Wrong publication ordering by title.", publication.getTitle(), orderedTitlesList.get(index));
            index++;
        }
    }

}
