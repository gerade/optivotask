package optivotask.reader;


import optivotask.model.Magazine;
import org.junit.Test;

import java.util.LinkedHashSet;
import java.util.Set;

import static junit.framework.Assert.*;

/**
 * Test for {@link MagazineReader}
 * <p/>
 * /data/zeitschriften.csv file is used for testing.
 *
 * @author Igor Polietaiev
 */
public class MagazineReaderTest {/**
 * Path to CSV file containing Magazines information.
 */
    /**
     * Path to CSV file containing magazines information.
     */
    private static final String MAGAZINES_FILENAME = "/data/zeitschriften.csv";
    private static final int NUMBER_OF_MAGAZINES = 6;

    @Test
    public void testMagazineReader() {
        Set<Magazine> magazines = new LinkedHashSet<Magazine>();
        try {
            MagazineReader magazineReader = new MagazineReader(MAGAZINES_FILENAME);
            magazines.addAll(magazineReader.readWithCsvBeanReader(Magazine.class));
        } catch (Exception e) {
            fail("Problem while parsing library csv files");
        }

        assertEquals("Wrong number of magazines was loaded.", NUMBER_OF_MAGAZINES, magazines.size());

        // test that all fields were set
        for (Magazine magazine : magazines) {

            assertNotNull("Magazines title was not set.", magazine.getTitle());
            assertNotNull("Magazines authors were not set.", magazine.getAuthors());
            assertNotNull("Magazines publication date was not set.", magazine.getPublicationDate());
            assertNotNull("Magazines ISBN was not set.", magazine.getIsbn());

        }

        // test that magazines from testing csv file are present in the collection
        Magazine magazine1 = new Magazine();
        magazine1.setIsbn("5454-5587-3210");
        assertTrue("Magazine with the following ISBN not found: " + magazine1.getIsbn(), magazines.contains(magazine1));

        Magazine magazine2 = new Magazine();
        magazine2.setIsbn("2365-5632-7854");
        assertTrue("Magazine with the following ISBN not found: " + magazine2.getIsbn(), magazines.contains(magazine2));

        Magazine magazine3 = new Magazine();
        magazine3.setIsbn("1313-4545-8875");
        assertTrue("Magazine with the following ISBN not found: " + magazine3.getIsbn(), magazines.contains(magazine3));

    }
}
