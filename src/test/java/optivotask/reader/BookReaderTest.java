package optivotask.reader;


import optivotask.model.Book;
import org.junit.Test;

import java.util.LinkedHashSet;
import java.util.Set;

import static junit.framework.Assert.*;

/**
 * Test for {@link BookReader}
 * <p/>
 * /data/buecher.csv file is used for testing.
 *
 * @author Igor Polietaiev
 */
public class BookReaderTest {
    /**
     * Path to CSV file containing books information.
     */
    private static final String BOOKS_FILENAME = "/data/buecher.csv";
    private static final int NUMBER_OF_BOOKS = 8;

    @Test
    public void testBookReading() {
        Set<Book> books = new LinkedHashSet<Book>();
        try {
            BookReader bookReader = new BookReader(BOOKS_FILENAME);
            books.addAll(bookReader.readWithCsvBeanReader(Book.class));
        } catch (Exception e) {
            fail("Problem while parsing library csv files");
        }

        assertEquals("Wrong number of books was loaded.", NUMBER_OF_BOOKS, books.size());

        // test that all fields were set
        for (Book book : books) {
            assertNotNull("Books title was not set.", book.getTitle());
            assertNotNull("Books authors were not set.", book.getAuthors());
            assertNotNull("Books description was not set.", book.getDescription());
            assertNotNull("Books ISBN was not set.", book.getIsbn());

        }

        // test that books from testing csv file are present in collection
        Book book1 = new Book();
        book1.setIsbn("5554-5545-4518");
        assertTrue("Book with the following ISBN not found: " + book1.getIsbn(), books.contains(book1));

        Book book2 = new Book();
        book2.setIsbn("2145-8548-3325");
        assertTrue("Book with the following ISBN not found: " + book2.getIsbn(), books.contains(book2));

        Book book3 = new Book();
        book3.setIsbn("4545-8558-3232");
        assertTrue("Book with the following ISBN not found: " + book3.getIsbn(), books.contains(book3));

    }
}
