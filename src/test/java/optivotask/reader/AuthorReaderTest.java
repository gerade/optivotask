package optivotask.reader;

import optivotask.model.Author;
import org.junit.Test;

import java.util.LinkedHashSet;
import java.util.Set;

import static junit.framework.Assert.*;

/**
 * Test for {@link AuthorReader}
 * <p/>
 * /data/autoren.csv file is used for testing.
 *
 * @author Igor Polietaiev
 */
public class AuthorReaderTest {
    /**
     * Path to CSV file containing authors information.
     */
    private static final String AUTHORS_FILENAME = "/data/autoren.csv";
    private static final int NUMBER_OF_AUTHORS = 6;

    @Test
    public void testAuthorReading() {
        Set<Author> authors = new LinkedHashSet<Author>();
        try {
            AuthorReader authorReader = new AuthorReader(AUTHORS_FILENAME);
            authors.addAll(authorReader.readWithCsvBeanReader(Author.class));


        } catch (Exception e) {
            fail("Problem while parsing library csv files");
        }

        assertEquals("Wrong number of authors was loaded.", NUMBER_OF_AUTHORS, authors.size());

        // test that all fields were set
        for (Author author : authors) {
            assertNotNull("Authors email was not set.", author.getEmail());
            assertNotNull("Authors name was not set.", author.getFirstName());
            assertNotNull("Authors lastname was not set.", author.getLastName());
        }

        // test that authors from testing csv file are present in collection
        Author author1 = new Author();
        author1.setEmail("pr-walter@optivo.de");
        author1.setFirstName("Paul");
        author1.setLastName("Walter");
        assertTrue("Author is missing: " + author1, authors.contains(author1));

        Author author2 = new Author();
        author2.setEmail("pr-ferdinand@optivo.de");
        author2.setFirstName("Franz");
        author2.setLastName("Ferdinand");
        assertTrue("Author is missing: " + author2, authors.contains(author2));

        Author author3 = new Author();
        author3.setEmail("pr-rabe@optivo.de");
        author3.setFirstName("Harald");
        author3.setLastName("Rabe");

        assertTrue("Author is missing: " + author3, authors.contains(author3));
    }
}
