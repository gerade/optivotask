package optivotask;

import optivotask.model.Author;
import optivotask.model.Book;
import optivotask.model.Magazine;
import optivotask.model.Publication;
import optivotask.reader.AuthorReader;
import optivotask.reader.BookReader;
import optivotask.reader.MagazineReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Class contains library collection initialization methods together with manipulation methods required by task.
 */
public class LibraryCollection {
    private Logger log = LoggerFactory.getLogger(LibraryCollection.class);
    /**
     * Path to CSV file containing authors information.
     */
    private static final String AUTHORS_FILENAME = "/data/autoren.csv";
    /**
     * Path to CSV file containing books information.
     */
    private static final String BOOKS_FILENAME = "/data/buecher.csv";
    /**
     * Path to CSV file containing magazines information.
     */
    private static final String MAGAZINES_FILENAME = "/data/zeitschriften.csv";
    /**
     * Separator used in CSV files to map list of values to one column.
     */
    private static final String LIST_SEPARATOR = ",";

    /**
     * {@link Author} cache.
     */
    Set<Author> authors = new HashSet<Author>();
    /**
     * {@link Book} cache.
     */
    Set<Book> books = new HashSet<Book>();
    /**
     * {@link Magazine} cache.
     */
    Set<Magazine> magazines = new HashSet<Magazine>();

    public LibraryCollection() {
        try {
            AuthorReader authorReader = new AuthorReader(AUTHORS_FILENAME);
            authors.addAll(authorReader.readWithCsvBeanReader(Author.class));

            BookReader bookReader = new BookReader(BOOKS_FILENAME);
            books.addAll(bookReader.readWithCsvBeanReader(Book.class));

            MagazineReader magazinesReader = new MagazineReader(MAGAZINES_FILENAME);
            magazines.addAll(magazinesReader.readWithCsvBeanReader(Magazine.class));

            matchAuthors();

        } catch (Exception e) {
            log.error("Problem while parsing library csv files", e);
        }
    }


    /**
     * Try to match author objects for all publications.
     */
    private void matchAuthors() {
        // create a map of author email and its instance for fast lookup
        Map<String, Author> emailToAuthorMap = new HashMap<String, Author>();
        for (Author author : authors) {
            emailToAuthorMap.put(author.getEmail(), author);
        }
        // match publication authors
        for (Publication publication : getAllPublications()) {
            List<Author> authorsList = new ArrayList<Author>();
            List<String> authorEmails = Arrays.asList(publication.getAuthors().split(LIST_SEPARATOR));
            for (String authorEmail : authorEmails) {
                Author author = emailToAuthorMap.get(authorEmail);
                if (author != null) {
                    authorsList.add(author);
                } else {
                    log.error("Author was not found for email: {}", authorEmail);
                }
            }
            publication.setAuthorsList(authorsList);
        }
    }

    /**
     * Get all publications.
     *
     * @return list of publications
     */
    public Set<Publication> getAllPublications() {
        Set<Publication> result = new HashSet<Publication>();
        result.addAll(books);
        result.addAll(magazines);
        return result;
    }

    /**
     * Get publication by given ISBN.
     *
     * @param isbn ISBN
     * @return publication with given ISBN or null if publication was not found
     */
    public Publication getPublicationByIsbn(String isbn) {
        for (Publication publication : getAllPublications()) {
            if (publication.getIsbn().equals(isbn)) {
                return publication;
            }
        }
        return null;
    }

    /**
     * Get set of publications by given {@link Author}.
     *
     * @param author
     * @return set of publication corresponding to given author. Empty list will be returned if author has no publications.
     */
    public Set<Publication> getPublicationByAuthor(Author author) {
        Set<Publication> result = new LinkedHashSet<Publication>();
        for (Publication publication : getAllPublications()) {
            if (publication.getAuthorsList().contains(author)) {
                result.add(publication);
            }
        }
        return result;
    }

    /**
     * Get list of all publications ordered by title.
     *
     * @return ordered list of publications
     */
    public Set<Publication> getAllPublicationsOrderedByTitle() {
        Set<Publication> result = new TreeSet<Publication>(new PublicationTitleComparator());
        result.addAll(getAllPublications());
        return result;
    }

    /**
     * Comparator used to compare publications by their title.
     */
    private class PublicationTitleComparator implements Comparator<Publication> {
        public int compare(Publication publication1, Publication publication2) {

            String publication1Title = publication1.getTitle();
            String publication2Title = publication2.getTitle();

            return publication1Title.compareTo(publication2Title);

        }
    }
}
