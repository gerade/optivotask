package optivotask;

import optivotask.model.Author;
import optivotask.model.Publication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello world!
 */
public class LibraryApplication {
    private static final Logger LOG = LoggerFactory.getLogger(LibraryApplication.class);

    public static void main(String[] args) {

        LibraryCollection libraryCollection = new LibraryCollection();

        LOG.info("Alle Bücher / Zeitschriften mit allen Details ausgeben");
        for (Publication publication : libraryCollection.getAllPublications()) {
            LOG.info(publication.toString());
        }

        LOG.info("Anhand einer ISBN-Nummer ein Buch / eine Zeitschrift finden und ausgeben");
        Publication publicationByIsbn = libraryCollection.getPublicationByIsbn("2365-8745-7854");
        if (publicationByIsbn != null) {
            LOG.info(publicationByIsbn.toString());
        }

        LOG.info("Alle Bücher / Zeitschriften eines Autors finden und ausgeben");
        Author author = new Author();
        author.setEmail("pr-walter@optivo.de");
        author.setFirstName("Paul"); // will not be used during search
        author.setLastName("Walter"); // will not be used during search
        for (Publication publication : libraryCollection.getPublicationByAuthor(author)) {
            LOG.info(publication.toString());
        }

        LOG.info("Alle Bücher / Zeitschriften nach Titel sortieren und ausgeben");
        for (Publication publication : libraryCollection.getAllPublicationsOrderedByTitle()) {
            LOG.info(publication.toString());
        }
    }


}
