package optivotask.model;

/**
 * Represents author entity.
 *
 * @author Igor Polietaiev
 */
public class Author {

    /**
     * Corresponds to Emailadresse in appropriate csv file.
     * Is a business key for authors.
     */
    private String email;

    /**
     * Corresponds to Vorname in appropriate csv file
     */
    private String firstName;

    /**
     * Corresponds to Nachname in appropriate csv file
     */
    private String lastName;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return firstName + ' ' +
                lastName + " (" + email + ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Author)) return false;

        Author author = (Author) o;

        if (!email.equals(author.email)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return email.hashCode();
    }

}
