package optivotask.model;

import java.util.List;

/**
 * Abstract class which holds common information for all publications (books, magazines).
 *
 * @author Igor Polietaiev
 */
public abstract class Publication {

    /**
     * Corresponds to Titel in appropriate csv file
     */
    String title;
    /**
     * Coma-separated string of authors. Corresponds to Autoren in appropriate csv file
     */
    String authors;
    /**
     * List of author objects for the publication
     */
    List<Author> authorsList;

    /**
     * Corresponds to ISBN-Nummer in appropriate csv file.
     * Is a business key for publications.
     */
    String isbn;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public List<Author> getAuthorsList() {
        return authorsList;
    }

    public void setAuthorsList(List<Author> authorsList) {
        this.authorsList = authorsList;
    }

    public String printAuthors() {
        StringBuilder builder = new StringBuilder();
        builder.append("Authors: ");
        if (authorsList != null) {
            for (int i = 0; i < authorsList.size(); i++) {
                builder.append(authorsList.get(i).toString());
                if (i != authorsList.size() - 1) {
                    builder.append(", ");
                }
            }
        }
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Publication)) return false;

        Publication that = (Publication) o;

        if (!isbn.equals(that.isbn)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return isbn.hashCode();
    }

}
