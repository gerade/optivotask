package optivotask.model;

/**
 * Represents magazine entity.
 *
 * @author Igor Polietaiev
 */
public class Magazine extends Publication {

    /**
     * Corresponds to Erscheinungsdatum in appropriate csv file
     */
    String publicationDate;

    public String getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    @Override
    public String toString() {
        return "Magazine: " + title + '\n' +
                printAuthors() + '\n' +
                "ISBN: " + isbn + '\n' +
                "Publication date: " + publicationDate;
    }

}
