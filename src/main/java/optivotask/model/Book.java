package optivotask.model;

/**
 * Represents book entity.
 *
 * @author Igor Polietaiev
 */
public class Book extends Publication {
    /**
     * Corresponds to Kurzbeschreibung in appropriate csv file
     */
    String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Title: " + title + '\n' +
                printAuthors() + '\n' +
                "ISBN: " + isbn + '\n' +
                "Description: " + description;
    }

}
