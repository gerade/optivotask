package optivotask.reader;

import optivotask.model.Author;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.constraint.StrRegEx;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.List;

/**
 * Reader to get authors information from csv file.
 *
 * @author Igor Polietaiev
 */
public class AuthorReader extends AbstractLibraryCsvReader {

    public AuthorReader(String inputFileName) {
        this.fileName = inputFileName;
    }

    /**
     * Custom dozer field mapping
     * @return mapping fields
     */
    @Override
    public String[] getFieldMapping() {
        return new String[]{
                "email",
                "firstName",
                "lastName"};
    }

    /**
     * Sets up the processors used for autoren.csv files. There are 3 CSV columns, so 3 processors are defined. Empty
     * columns are read as null (hence the NotNull() for mandatory columns).
     *
     * @return the cell processors
     */
    @Override
    public CellProcessor[] getProcessors() {

        final String emailRegex = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
        StrRegEx.registerMessage(emailRegex, "must be a valid email address");

        final CellProcessor[] processors = new CellProcessor[]{
                new StrRegEx(emailRegex), //email
                new NotNull(), // firstName
                new NotNull() // lastName
        };

        return processors;
    }

}
