package optivotask.reader;

import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;


/**
 * Reader to get books information from csv file.
 *
 * @author Igor Polietaiev
 */
public class BookReader extends AbstractLibraryCsvReader {

    public BookReader(String inputFileName) {
        this.fileName = inputFileName;
    }

    @Override
    public String[] getFieldMapping() {
        return new String[]{
                "title",
                "isbn",
                "authors",
                "description"};
    }

    /**
     * Sets up the processors used for autoren.csv files. There are 4 CSV columns, so 4 processors are defined. Empty
     * columns are read as null (hence the NotNull() for mandatory columns).
     *
     * @return the cell processors
     */
    @Override
    public CellProcessor[] getProcessors() {
        final CellProcessor[] processors = new CellProcessor[]{
                new NotNull(), // title
                new NotNull(), // ISBN
                new NotNull(), // coma separated string of authors
                new NotNull() // description
        };
        return processors;
    }

}
