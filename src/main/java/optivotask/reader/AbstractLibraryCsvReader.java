package optivotask.reader;

import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.dozer.CsvDozerBeanReader;
import org.supercsv.io.dozer.ICsvDozerBeanReader;
import org.supercsv.prefs.CsvPreference;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


/**
 * Abstract reader class to get information from different csv files.
 * Type of the entity should be provided.
 *
 * @author Igor Polietaiev
 */
public abstract class AbstractLibraryCsvReader<T> {
    private static final CsvPreference SEMICOLON_DELIMITED_PREFERENCES = new CsvPreference.Builder('"', ';', "\n").build();
    protected String fileName;

    /**
     * Custom dozer field mapping
     *
     * @return mapping fields
     */
    public abstract String[] getFieldMapping();

    /**
     * Sets up the processors used for csv parsing files.
     *
     * @return the cell processors
     */
    public abstract CellProcessor[] getProcessors();

    /**
     * Path to csv file to parse.
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Read data using CsvBeanReader from SuperVSV library.
     *
     * @param type entity type
     * @return list of read entities
     * @throws Exception
     */
    public Set<T> readWithCsvBeanReader(Class<T> type) throws Exception {
        Set<T> result = new LinkedHashSet<T>();
        ICsvDozerBeanReader beanReader = null;
        try {
            InputStreamReader inputStreamReader = new InputStreamReader(this.getClass().getResourceAsStream(getFileName()));
            beanReader = new CsvDozerBeanReader(inputStreamReader, SEMICOLON_DELIMITED_PREFERENCES);

            beanReader.getHeader(true); // ignore the header
            beanReader.configureBeanMapping(type, getFieldMapping());

            T author;
            while ((author = beanReader.read(type, getProcessors())) != null) {
                result.add(author);
            }

        } finally {
            if (beanReader != null) {
                beanReader.close();
            }
        }
        return result;
    }
}
